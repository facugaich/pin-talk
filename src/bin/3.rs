
#[derive(Debug)]
struct AutoReferential<'a> {
    data: String,
    substring: &'a str,
}

impl<'a> AutoReferential<'a> {
    fn new(data: String, length: usize) -> Self {
        let substring = &data[..length];

        Self { data, substring }
    }
}

fn main() {

}
