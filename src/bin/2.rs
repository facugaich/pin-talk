
#[derive(Debug)]
struct Nice {
    foo: u32,
    bar: f64,
}

fn f(mut s: Nice) -> Nice {
    s.foo = s.foo * (s.bar as u32);
    s.bar = s.foo as f64;

    let p: *const Nice = &s;

    println!("{:?}, located at {:?}", s, p);

    return s;
}

fn main() {

    let s = Nice {
        foo: 10,
        bar: 3.14,
    };

    let p: *const Nice = &s;

    println!("{:?}, located at {:?}", s, p);

    let s = f(s);

    let p: *const Nice = &s;

    println!("{:?}, located at {:?}", s, p);
}
