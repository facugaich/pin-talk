use std::slice;
use std::fmt;

struct AutoReferential<'a> {
    data: [u32; 3],
    subdata: &'a [u32],
}

impl<'a> AutoReferential<'a> {
    fn new(first: u32, second: u32, third: u32) -> Self {
        let mut data = [0; 3];
        data[0] = first;
        data[1] = second;
        data[2] = third;

        Self { data, subdata: &[0; 3] }
    }

    fn init_unsafe_ref(&mut self, length: usize) {
        let data_ptr = self.data.as_ptr();

        unsafe {
            self.subdata = slice::from_raw_parts(data_ptr, length);
        }
    }
}


fn main() {

    let mut a = AutoReferential::new(1, 2, 3);
    a.init_unsafe_ref(2);
    let mut b = AutoReferential::new(4, 5, 6);
    b.init_unsafe_ref(3);

    println!("AutoReference a:\n{:?}", a);
    println!("AutoReference b:\n{:?}", b);

    println!("<-- SWAP -->");
    std::mem::swap(&mut a, &mut b);

    println!("AutoReference a:\n{:?}", a);
    println!("AutoReference b:\n{:?}", b);
}

impl<'a> fmt::Debug for AutoReferential<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let p: *const u32 = self.data.as_ptr();
        f.write_fmt(format_args!("Data {:?} is located at {:?}\n", self.data, p))?;
        let p: *const u32 = self.subdata.as_ptr();
        f.write_fmt(format_args!("Subslice {:?} is located at {:?}", self.subdata, p))
    }
}
