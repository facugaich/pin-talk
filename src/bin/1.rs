
#[derive(Debug)]
struct Nice {
    foo: u32,
    bar: f64,
}

fn main() {

    let s = Nice {
        foo: 10,
        bar: 3.14,
    };

    let p: *const Nice = &s;

    println!("{:?}, located at {:?}", s, p);

    let b = Box::new(s);

    let p: *const Nice = Box::into_raw(b);

    println!("Now located at {:?}", p);
}
