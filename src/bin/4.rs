use std::slice;

#[derive(Debug)]
struct AutoReferential<'a> {
    data: [u32; 3],
    subdata: &'a [u32],
}

impl<'a> AutoReferential<'a> {
    fn new(first: u32, second: u32, third: u32) -> Self {
        let mut data = [0; 3];
        data[0] = first;
        data[1] = second;
        data[2] = third;

        Self { data, subdata: &[0; 3] }
    }

    fn init_unsafe_ref(&mut self, length: usize) {
        let data_ptr = self.data.as_ptr();

        unsafe {
            self.subdata = slice::from_raw_parts(data_ptr, length);
        }
    }
}

fn main() {

    let mut a = AutoReferential::new(1, 2, 3);
    a.init_unsafe_ref(2);

    println!("{:?}", a);

    let p: *const u32 = a.data.as_ptr();

    println!("{:?} is located at {:?}", a.data, p);

    let p: *const u32 = a.subdata.as_ptr();

    println!("{:?} is located at {:?}", a.subdata, p);

}
